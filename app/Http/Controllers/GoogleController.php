<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
    public function redirectToProvider() {
		$response = [
			'redirectUrl' => Socialite::with('google')->stateless()->redirect()->getTargetUrl(),
		];
		return $this->successResponse($response, "Redirected Login Google");
	}

	public function handleProviderCallback() {
		try {
			$user = Socialite::with('google')->stateless()->user();
		} catch (HttpClientException $exception) {
			return $this->errorResponse("Invalid credentials provided.", [], 422);
		}

		$userCreated = User::firstOrCreate(
			[
				'email' => $user->getEmail()
			],
			[
				'password' => bcrypt(uniqid()),
				'email_verified_at' => now(),
				'name' => $user->getName(),
				'status' => true,
			]
		);

		$userCreated->providers()->updateOrCreate(
			[
				'provider' => 'google',
				'provider_id' => $user->getId(),
			],
			[
				'avatar' => $user->getAvatar()
			]
		);
		$response = [
			'token' => $userCreated->createToken('sheva')->plainTextToken,
			'user' => $userCreated
		];
		return $this->successResponse($response, "User Successfully Login");
	}
}
