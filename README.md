# Clone Respository
`git clone https://gitlab.com/shevaathalla/larastock.git`

# Go to directory
`cd larastock`

# Install laravel
`composer install`

# Copy .env
`cp .env.example .env`
- Then create database and edit database in .env
- add Mail enviroment to send email when reset password 
- Then fill GOOGLE_CLIENT_ID GOOGLE_CLIENT_SECRET GOOGLE_REDIRECT_URI with credentians google in .env

# Generate Key
`php artisan key:generate`

# Migrate and seed database
`php artisan migrate --seed`

# Run Laravel 
`php artisan serve`

# Documentation Postman
https://documenter.getpostman.com/view/15292396/Tzm2KyS8