<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\GoogleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NewPasswordController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('user', function (Request $request) {
    return $request->user();
});
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login'])->name('login');
Route::post('forgot-password', [NewPasswordController::class, 'forgotPassword']);
Route::post('reset-password', [NewPasswordController::class, 'reset'])->name('password.reset');
Route::get('login/google', [GoogleController::class, 'redirectToProvider']);
Route::get('login/google/callback', [GoogleController::class, 'handleProviderCallback']);

Route::middleware('auth:sanctum')->group(function() {
	Route::post('logout', [AuthController::class, 'logout']);

	Route::apiResource('blog', BlogController::class)->except([
		'index', 'show'
	]);
	Route::apiResource('college', CollegeController::class)->except([
		'index', 'show'
	]);
	Route::post('college/{college}/major/', [CollegeController::class, 'storeMajor']);
	
	Route::patch('college/{college}/major/{major}', [CollegeController::class, 'updateMajor']);
	Route::delete('college/{college}/major/{major}', [CollegeController::class, 'destroyMajor']);
});